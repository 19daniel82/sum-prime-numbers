package it.danielguarise.primenumbers;

import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author Daniel Guarise
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            scanner.useDelimiter(System.getProperty("line.separator"));
            System.out.println("Type how many prime numbers you want to sum, then press enter to continue...");
            int k = scanner.nextInt();
            System.out.println(String.format("The sum of the first %d prime numbers is %d", k, sumThePrimeNumbers(k)));
        } catch (NoSuchElementException | IllegalStateException exc) {
            System.out.println(String.format("Your input data has caused something really bad -> %s", exc));
        }
    }

    static long sumThePrimeNumbers(int k) {
        long result = 0;
        int counter = 0;

        for (int n = 2; counter < k; n++) {
            if (isPrimeNumber(n)) {
                result += n;
                counter++;
            }
        }

        return result;
    }

    static boolean isPrimeNumber(int n) {
        boolean isPrime = true;

        //All numbers minor or equals 1 are not prime by definition
        if (n <= 1) {
            isPrime = false;
        }

        //If n is greater than 3 (2 and 3 are prime)
        if (n > 3) {
            //Check if n is divisible by 2 or 3
            if (n % 2 == 0 || n % 3 == 0) {
                isPrime = false;
            }

            //Check if n is divisible by x = 6k ± 1, with k>=1 and until x <= √n
            for (int x = 5; isPrime && x * x <= n; x += 6) {
                if (n % x == 0 || n % (x + 2) == 0) {
                    isPrime = false;
                }
            }
        }

        return isPrime;
    }

}
