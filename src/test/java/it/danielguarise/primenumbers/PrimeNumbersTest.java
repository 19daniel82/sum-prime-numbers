/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.danielguarise.primenumbers;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Daniel Guarise
 */
public class PrimeNumbersTest {

    @Test
    public void checkPrimeNumbers() {
        Assert.assertFalse("-1 is not a prime number", Main.isPrimeNumber(-1));
        Assert.assertFalse("0 is not a prime number", Main.isPrimeNumber(0));
        Assert.assertFalse("1 is not a prime number", Main.isPrimeNumber(1));
        Assert.assertTrue("2 is a prime number", Main.isPrimeNumber(2));
        Assert.assertTrue("3 is a prime number", Main.isPrimeNumber(3));
        Assert.assertFalse("4 is not a prime number", Main.isPrimeNumber(4));
        Assert.assertTrue("5 is a prime number", Main.isPrimeNumber(5));
        Assert.assertFalse("6 is not a prime number", Main.isPrimeNumber(6));
        Assert.assertTrue("7 is a prime number", Main.isPrimeNumber(7));
        Assert.assertFalse("8 is not a prime number", Main.isPrimeNumber(8));
        Assert.assertFalse("9 is not a prime number", Main.isPrimeNumber(9));
        Assert.assertFalse("10 is not a prime number", Main.isPrimeNumber(10));
        Assert.assertTrue("11 is a prime number", Main.isPrimeNumber(11));
        Assert.assertFalse("12 is not a prime number", Main.isPrimeNumber(12));
        Assert.assertTrue("13 is a prime number", Main.isPrimeNumber(13));
        Assert.assertFalse("14 is not a prime number", Main.isPrimeNumber(14));
        Assert.assertFalse("15 is not a prime number", Main.isPrimeNumber(15));
    }
    
    @Test
    public void sumOfTheFirstKPrimeNumbers() {
        Assert.assertEquals("The sum of the first 0 prime numbers must be 0", 0, Main.sumThePrimeNumbers(0));
        Assert.assertEquals("The sum of the first 1 prime numbers must be 2", 2, Main.sumThePrimeNumbers(1));
        Assert.assertEquals("The sum of the first 2 prime numbers must be 5", 5, Main.sumThePrimeNumbers(2));
        Assert.assertEquals("The sum of the first 3 prime numbers must be 10", 10, Main.sumThePrimeNumbers(3));
        Assert.assertEquals("The sum of the first 5 prime numbers must be 28", 28, Main.sumThePrimeNumbers(5));
        Assert.assertEquals("The sum of the first 10 prime numbers must be 129", 129, Main.sumThePrimeNumbers(10));
    }

}
